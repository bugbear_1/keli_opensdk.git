<?php
namespace Keli\OpenSDK\EcommercePay;


use Keli\OpenSDK\Core\Api;

class EcommercePay extends Api{
    /**
     * 商圈消息解密
     * @param String $out_trade_no
     * @param String $sub_mch_id
     * @param String $subject
     * @param String $openid
     * @return mixed
     */
    public function aesDecrypt(String $associatedData, String $nonceStr, String $ciphertext){
        return $this->request('pay/wechatPay/ecommercePay/aesDecrypt',[
            'associatedData' => $associatedData,
            'nonceStr' => $nonceStr,
            'ciphertext' => $ciphertext
        ]);
    }
}